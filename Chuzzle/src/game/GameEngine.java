package game;


import java.awt.Color;
import java.awt.Font;
import javax.swing.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.logging.Level;
import java.util.logging.Logger;



public class GameEngine implements MouseMotionListener, MouseListener{

    private final int array_size = 64;
    private final int width = 8, heigth = 8;
    private Square[] sa = new Square[array_size];
    public GameWindow gw = new GameWindow();

    //Position variables
    private int pressed_x, pressed_y;
    private int dragged_x, dragged_y;
    private int pressedPosition;
    private int row, column;

    //Shifting variables
    private int left_position;
    private int right_position;
    private int top_position;
    private int bottom_position;


    //Event booleans
    boolean mousePressed = false;
    boolean mouseDragged = false;
    
    //Turnos
    private int turno=0;
    public int level=1;
    
    //Puntuación
    public JButton salir= new JButton();
    public JTextField puntuacion=new JTextField();
    public JLabel LabelScore=new JLabel();
    public JLabel fondo= new JLabel();
    public JLabel reloj=new JLabel();
    public JLabel panel=new JLabel();
    public Score score=new Score();
    
    public Square[] aux= new Square[sa.length];
    public Reproductor reproductor=new Reproductor();
    public int scoreant=-1;
    public boolean band=true;
    

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }
    
    public void setNombre(String nom){
        System.out.println(nom);
        this.score.setNom(nom);
    }
/*Inicialisamos todas las etiquetas, fuentes, cuadros de texto, que se van a utilizar*/
    //Les agregamos su icono, color, diseño, estilo
    public void initializeLabels(){
        Font fuente=new Font("Dialog", Font.BOLD, 36);
        Font fuente2=new Font("Dialog", Font.BOLD, 20);
        puntuacion.setFont(fuente);
        puntuacion.setText("000");
        puntuacion.setBackground(Color.LIGHT_GRAY);
        fondo.setIcon(new ImageIcon("./img/fondof.jpg"));
        panel.setIcon(new ImageIcon("./img/Gears1.gif"));
        LabelScore.setIcon(new ImageIcon("./img/score2.png"));
        reloj.setIcon(new ImageIcon("./img/Gears1.gif"));
        salir.setText("Salir");
        salir.setFont(fuente2);
        salir.setSize(40,60);
        for(int i=0; i < array_size; i++){
            sa[i] = new Square();
            sa[i].setIcon(new ImageIcon(getRandomImage()));
            sa[i].setPosition(i);
            sa[i].addMouseMotionListener(this);
            sa[i].addMouseListener(this);
        }
    }
//Generamos la ventana
    public void createWindow() throws Exception{
        
        reproductor.AbrirFichero("src/Archivos/Music3.mp3");
        reproductor.Play();
        gw.setSquares(sa); //Genera todas las bolitas del juego 
        gw.setExtras(puntuacion,LabelScore,reloj,fondo,panel,salir,reproductor);//Genera todo lo extra del frame
    }

    @Override
    public void mousePressed(MouseEvent me) {//Acciones al presionar el mouse
        aux=sa;
        if(turno>=5&&turno<19){
            reloj.setIcon(new ImageIcon("./img/Gears3.gif"));
            reloj.setBounds(90,300,250,340);
        }
        /*if(turno>=15&&turno<20){
            reloj.setIcon(new ImageIcon("./img/Gears3.gif"));
        }*/
        /*if(turno>=15&&turno<20){
            reloj.setIcon(new ImageIcon("./img/Gears4.gif"));
            reloj.setBounds(90,270,400,440);
        }*/
        if(turno==19){
            reloj.setBounds(10,270,400,440);
            reloj.setIcon(new ImageIcon("./img/engrane.gif"));
        }
        if (turno == 20) {//Indica el fin del juego
            
            score.equals(Pop.puntos);
            //fondo.setBounds(0, 0, 1100, 800);
            String n = "Fin del juego\n Puntuacion final de ";
            if (score.getNom() == null) {
                n += " Desconocido: ";
            } else {
                n += score.getNom()+" ";
            }
            n += score.getScore();
            JOptionPane.showMessageDialog(null, n);//Te devuelve tu puntuación 
            gw.dispose();
            try {
                reproductor.Stop();
            } catch (Exception ex) {
                Logger.getLogger(GameEngine.class.getName()).log(Level.SEVERE, null, ex);
            }
            Archivo ar= new Archivo();
            ar.setFuente("src/Scoref.txt");
            ar.NuevoJugador(score);

        }
        pressedPosition = ((Square)me.getSource()).getPosition();
        pressed_x = me.getX();
        pressed_y = me.getY();
        //System.out.println("mousePressed - Position: " + pressedPosition + " - X: " + pressed_x + " - Y: " + pressed_y);
        mousePressed = true;
    }

    @Override
    public void mouseDragged(MouseEvent me) {//genera el movimiento al arrastrar el mouse
        //System.out.println("mouseDragged - X: " + me.getX() + " - Y: " + me.getY());
        dragged_x = me.getX();
        dragged_y = me.getY();
        mouseDragged = true;

        /*if( (dragged_x - pressed_x) % 80 == 0 ){
            //System.out.println("**********SHIFT RIGHT IMAGES!!!*****************");
            if(dragged_x > 0) {
                shiftRight();
            }
            else{
                shiftLeft();
            }
        }*/
    }

    @Override
    public void mouseMoved(MouseEvent me) {//hacemos las validaciones mientras el mouse se mueva
        //System.out.println("mouseMoved");
        if(turno==0){//genera un conjunto de bolitas no repetidas para empezar el juego
                sa=Pop.New(sa);
                turno++;
        }
        else{
        if(CheckPop(sa)==true){//revisamos si nuestro conjunto de bolitas esta lleno
            try {
                sa=Gravity.RunGravity(sa,level);//bajamos todas las bolitas y llenamos la primera fila
            } catch (Exception ex) {
                Logger.getLogger(GameEngine.class.getName()).log(Level.SEVERE, null, ex);
            }
            try{Thread.sleep(200);}catch(Exception e){}
            
        }
        else
            sa=Pop.Corre(sa,score);//validamos si hay mas de 3 bolitas juntas y las eliminamos
        }
        puntuacion.setText(""+score.getScore());
    }

    @Override
    public void mouseReleased(MouseEvent me) {//acciones
        mousePressed = false;
        mouseDragged = false;
        if(turno>1){
            scoreant=score.getScore();
        }
            
        try{Thread.sleep(100);}catch(Exception e){}
        sa=Pop.Corre(sa,score);
        puntuacion.setText(""+score.getScore());
            
        System.out.println("Anterior: "+scoreant);
        System.out.println("Ahora: "+score.getScore());
        if(scoreant==score.getScore()){
            sa=aux;
        }
        turno++;
        
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        //System.out.println("mouseEntered");
    }

    @Override
    public void mouseExited(MouseEvent me) {
        //System.out.println("mouseExited");
        int move_in_x = 0;
        int move_in_y = 0;
        if(mousePressed && mouseDragged) {

            //Determine the biggest difference to know if movement was on x or y
            move_in_x = Math.abs(pressed_x - dragged_x);
            move_in_y = Math.abs(pressed_y - dragged_y);

            if(move_in_x > move_in_y) {
                if (pressed_x > dragged_x) {
                    shiftLeft();
                    //System.out.println("Shifting left");
                }
                //if (pressed_x < dragged_x) {
                else{
                    shiftRight();
                    //System.out.println("Shifting right");
                }
            }
            else{
                if (pressed_y > dragged_y){
                    shiftUp();
                }
                //if (pressed_y < dragged_y) {
                else{
                    shiftDown();
                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        //System.out.println("mouseClicked");
        //System.out.println("Calling Swap");

        //System.out.println("Label position:" + ((Square)me.getSource()).getPosition());
        //gw.swap(((Square)me.getSource()).getPosition());
        //System.out.println("Called Swap");
    }


    //Private methods
    private String getRandomImage(){
        int index = (int) Math.floor(Math.random() * 9);
        //System.out.println("index: " + index);
        switch (index) {
            case 1:
                return "./img/b1.png";
            case 2:
                return "./img/Piedra.png";
            case 3:
                return "./img/Metal.png";
            case 4:
                return "./img/Agua.png";
            case 5:
                return "./img/BolaDePapel.png";
            case 6:
                return "./img/bola.png";
            case 7: 
                return "./img/bola2.png";
            case 8:
                return "./img/bola3.png";
            default:
                return "./img/Madera.png";
        }
    }

    public void shiftRight(){
        getXShiftPositions();


        Square temp = new Square();
        temp.setIcon(sa[right_position].getIcon());
        for(int i = right_position - 1; i >= left_position; i--){
            sa[i+1].setIcon(sa[i].getIcon());
        }
        sa[left_position].setIcon(temp.getIcon());
    }

    public void shiftLeft(){
        getXShiftPositions();

        Square temp = new Square();
        temp.setIcon(sa[left_position].getIcon());
        for(int i = left_position; i < right_position; i++){
            sa[i].setIcon(sa[i+1].getIcon());
        }
        sa[right_position].setIcon(temp.getIcon());
    }

    public void shiftUp(){
        getYShiftPositions();

        Square temp = new Square();
        temp.setIcon(sa[bottom_position].getIcon());
        for(int i = bottom_position; i < top_position; i += heigth){
            sa[i].setIcon(sa[i + heigth].getIcon());
        }
        sa[top_position].setIcon(temp.getIcon());
    }

    public void shiftDown(){
        getYShiftPositions();

        Square temp = new Square();
        temp.setIcon(sa[top_position].getIcon());
        for(int i = top_position; i > bottom_position; i -= heigth){
            sa[i].setIcon(sa[i - heigth].getIcon());
        }
        sa[bottom_position].setIcon(temp.getIcon());
    }

    private void getXShiftPositions(){
        //Determine the row to be shifted
        if(pressedPosition == 0){
            row = 0;
        }else{
            row = pressedPosition / width;
        }

        //Determine ranges
        if(row == 0){
            left_position = 0;
            right_position = width - 1;
        }
        else{
            left_position = row * width;
            right_position = left_position + width - 1;
        }
    }

    private void getYShiftPositions(){
        int index = pressedPosition;

        while(index >= 0){
            index = index - heigth;
        }
        bottom_position = index + heigth;
        //System.out.println("Bottom position: " + bottom_position);

        index = pressedPosition;
        while(index < array_size){
            index = index + heigth;
        }
        top_position = index - heigth;
        //System.out.println("Top position: " + top_position);
    }

    private boolean CheckPop(Square[] sa) {
        for (int i = 0; i < sa.length-1; i++) {
            if(sa[i].getIcon()==null)
            //if(sa[i].getIcon().toString().equals("./img/broja.png"))
                return true;
            
                
        }
        return false;
    }


    
}