
package game;

import javax.swing.JOptionPane;



public class Launcher {
    public static void main(String args[]) throws Exception{
        
        GameEngine ge = new GameEngine();
        ge.initializeLabels();
        ge.createWindow();
        ge.setNombre(JOptionPane.showInputDialog(null, "Nombre: "));
    }
}
