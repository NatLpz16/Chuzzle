package game;

import javax.swing.*;
import java.awt.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GameWindow extends JFrame {
    GameWindow g;
    private int padding_left = 500;
    private int padding_top = 100;

    public GameWindow(){
        super("Chuzzle");
        this.setResizable(false);
        this.setPreferredSize(new Dimension(1300, 800));
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.getContentPane().setLayout(null);
        this.getContentPane().setBackground(Color.orange);
        g=this;
        
    }

    public void setSquares(Square[] sa){
        for (int i = 0; i < 57; i += 8) {
            for (int j = i; j < i + 8; j++) {
                this.getContentPane().add(sa[j],BorderLayout.SOUTH);
                sa[j].setBounds(padding_left, padding_top, 80, 80);
                padding_left += 80;

            }
            padding_left = 500;
            padding_top += 80;
        }
    }
    public void setExtras(JTextField et, JLabel s, JLabel r, JLabel f, JLabel p, JButton salir, Reproductor rep){
        this.getContentPane().add(salir,BorderLayout.SOUTH);
        salir.setBounds(150,690,100,60);
        this.getContentPane().add(et,BorderLayout.NORTH);
        et.setBounds(140, 170, 90, 40);
        this.getContentPane().add(s,BorderLayout.NORTH);
        s.setBounds(50, 50, 300, 140);
        this.getContentPane().add(r,BorderLayout.NORTH);
        r.setBounds(120,300,250,340);
        this.getContentPane().add(f,BorderLayout.NORTH);
        f.setBounds(0, 0, 1300, 800);
        this.getContentPane().add(p,BorderLayout.NORTH);
        p.setBounds(80,300,250,340);
        salir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                g.dispose();
                try {
                    rep.Stop();
                } catch (Exception ex) {
                    Logger.getLogger(GameWindow.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
      
    }
    

    public void shiftRight(){

    }


    public void swap(int position){
        //System.out.println("On Swap");
        //Label temp = (Label)container.getComponent(position);
        //container.add(container.getComponent(position+1), BorderLayout.SOUTH, position);
        //container.add(temp, BorderLayout.SOUTH, position+1);
        //container.remove(0);
        //container.remove(1);
        //container.add(pclass[1].label,BorderLayout.SOUTH,0);
        //container.add(pclass[0].label,BorderLayout.SOUTH,0);
        //container.remove(position+1);
        /*Label current = (Label)container.getComponent(position);
        System.out.println(current.getIcon().toString());
        Label next = (Label)container.getComponent(position+1);
        System.out.println(next.getIcon().toString());
        Label temp = new Label();
        temp.setIcon(next.getIcon());

        next.setIcon(current.getIcon());
        current.setIcon(temp.getIcon());*/
        Square l0 = (Square)getContentPane().getComponent(0);
        Square l1 = (Square)getContentPane().getComponent(1);
        Square l2 = (Square)getContentPane().getComponent(2);
        Square l3 = (Square)getContentPane().getComponent(3);
        Square l4 = (Square)getContentPane().getComponent(4);

        /*Square temp = new Square();

        temp.setIcon(l1.getIcon());
        l1.setIcon(l0.getIcon());
        l2.setIcon(temp.getIcon());

        temp.setIcon(l3.getIcon());
        l3.setIcon(l2.getIcon());
        l4.setIcon(temp.getIcon());*/

        Icon[] a = new Icon[5];
        a[0] = l4.getIcon();
        a[1] = l0.getIcon();
        a[2] = l1.getIcon();
        a[3] = l2.getIcon();
        a[4] = l3.getIcon();

        for(int i=0;i<5;i++){
            Square s = (Square)getContentPane().getComponent(i);
            s.setIcon(a[i]);
        }

        getContentPane().validate();
        getContentPane().repaint();
        //System.out.println("Done Swap");
    }

}
