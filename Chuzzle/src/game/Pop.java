/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import java.util.ArrayList;
import javax.swing.ImageIcon;

public class Pop {
    public int contP;
    public static int puntos;
    
    public Pop() {
        contP=0;
    }
    
    
    public static Square[] Corre(Square[] sa, Score score) {
        //ArrayList <Integer> lista= new ArrayList();
        Pop p= new Pop();
        puntos=0;
        int [][]mat=new int[8][8];
        int []vec=new int[sa.length];
        for (int k = 0; k < vec.length; k++) {
            if(sa[k].getIcon()!=null){
            switch(sa[k].getIcon().toString()){
                case "./img/b1.png":
                    vec[k]=1;
                    break;
                case "./img/Piedra.png":
                    vec[k]=2;
                    break;
                case "./img/Metal.png":
                    vec[k]=3;
                    break;
                case "./img/Agua.png":
                    vec[k]=4;
                    break;
                case "./img/BolaDePapel.png":
                    vec[k]=5;
                    break;
                case "./img/Madera.png":
                    vec[k]=6;
                    break;
                case "./img/bola.png":
                    vec[k]=7;
                    break;
                case "./img/bola2.png":
                    vec[k]=8;
                    break;
                case "./img/bola3.png":
                    vec[k]=9;
                    break;
                case "./img/broja.png":
                    vec[k]=10;
                    break;
        }}
        }
            int cont=0;
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                mat[i][j]= vec[cont];
                cont++;
                
            }
        }
       // int pos=0;
        /*for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                System.out.print("["+mat[i][j]+"] ");
            }
            System.out.println();
        }*/
        /*mat[0][0]=1;
        mat[0][1]=1;
        mat[1][0]=1;*/
        
        recursivo(0,0,mat);
        adyacentesNegativos(0,0,mat);
        /*System.out.println();System.out.println();
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                System.out.print("["+mat[i][j]+"] ");
            }
            System.out.println();
        }*/
        sa=elimina(sa,mat);
        score.setPuntuacion(puntos);
        return sa;
        
    }
    
    public static void recursivo(int i,int j,int [][]mat){
        /*Evalúa si la casilla tiene alrededor 2 iguales*/
        int contador=0;
        //System.out.println("revisando i="+1+"   j="+j);
        if(i==8){
            
        }else{
            if(j==8){
                j=0;
                i=i+1;
                recursivo(i,j,mat);
            }else{
                if(i!=0){
                    if(mat[i][j]==mat[i-1][j] || mat[i][j]*(-1)==mat[i-1][j]){
                        //System.out.println("conte arriba");
                        contador++;
                    }
                }
                if(j!=0){
                    if(mat[i][j]==mat[i][j-1] || mat[i][j]*(-1)==mat[i][j-1]){
                        //System.out.println("conte izquierda");
                        contador++;
                    }
                }
                if(i!=7){
                    if(mat[i][j]==mat[i+1][j] || mat[i][j]*(-1)==mat[i+1][j]){
                        //System.out.println("conte abajo");
                        contador++;
                    }
                }
                if(j!=7){
                    if(mat[i][j]==mat[i][j+1] || mat[i][j]*(-1)==mat[i][j+1]){
                        //System.out.println("conte derecha");
                        contador++;
                    }
                }
                //System.out.println("-----------------------------");
                if(contador>1){
                    convierteAlrededores(i,j,mat);
                }
                recursivo(i,j+1,mat);
            }
        }
    }
    
    public static void convierteAlrededores(int i,int j,int [][]mat){
        if(mat[i][j]>0){
            mat[i][j]=mat[i][j]*(-1);
        }
        
        if(i!=0){
            if(mat[i][j]==mat[i-1][j] || mat[i][j]*(-1)==mat[i-1][j]){
                if(mat[i-1][j]>0){
                    mat[i-1][j]=mat[i-1][j]*(-1);     
                }
            }
        }
        if(j!=0){
            if(mat[i][j]==mat[i][j-1] || mat[i][j]*(-1)==mat[i][j-1]){
                if(mat[i][j-1]>0){
                    mat[i][j-1]=mat[i][j-1]*(-1);
                }
            }
        }
        if(i!=7){
            if(mat[i][j]==mat[i+1][j] || mat[i][j]*(-1)==mat[i+1][j]){
                if(mat[i+1][j]>0){
                    mat[i+1][j]=mat[i+1][j]*(-1);
                }
            }
        }
        if(j!=7){
            if(mat[i][j]==mat[i][j+1] || mat[i][j]*(-1)==mat[i][j+1]){
                if(mat[i][j+1]>0){
                    mat[i][j+1]=mat[i][j+1]*(-1);
                }
            }
        }
    }

   
    
    public static void adyacentesNegativos(int i,int j,int [][]mat){
        /*Revisa los numeros negativos para ver si hay otro numemro a lado positivo y lo convierte*/
        if(i==8){
            
        }else{
            if(j==8){
                j=0;
                i=i+1;
                recursivo(i,j,mat);
            }else{
                if(mat[i][j]<0){
                    if(i!=0){
                        if(mat[i][j]*(-1)==mat[i-1][j]){
                            mat[i-1][j]=mat[i-1][j]*(-1);
                        }
                    }
                    if(j!=0){
                        if(mat[i][j]*(-1)==mat[i][j-1]){
                            mat[i][j-1]=mat[i][j-1]*(-1);
                        }
                    }
                    if(i!=7){
                        if(mat[i][j]*(-1)==mat[i+1][j]){
                            mat[i+1][j]=mat[i+1][j]*(-1);
                        }
                    }
                    if(j!=7){
                        if(mat[i][j]*(-1)==mat[i][j+1]){
                            mat[i][j+1]=mat[i][j+1]*(-1);
                        }
                    }
                }
                
                recursivo(i,j+1,mat);
            }
        }
        
    }
    private static Square[] elimina(Square[] sa, int[][] mat) {
        int []vec=new int[sa.length];
        int cont=0;
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                vec[cont]=mat[i][j];
                cont++;
            }
        }
        for (int i = 0; i < vec.length; i++) {
            if(vec[i]<0){
                puntos+=10;
                /*if(sa[i].getIcon().toString().equals("./img/broja.png")){
                    sa[i+1].setIcon(null);
                    sa[i-1].setIcon(null);
                    sa[i+8].setIcon(null);
                    sa[i-8].setIcon(null);
                }*/
                    
                sa[i].setIcon(null);
                //sa[i].setIcon(new ImageIcon("./img/broja.png"));
                //System.out.println("Eliminando");
            }
        }
        return sa;
    }
        
    public static Square[] New(Square[] sa){
        for (int i = 0; i < sa.length; i++) {
            sa[i].setIcon(new ImageIcon(getRandomImage()));
        }
        for (int j = 0; j < 3; j++) {
            for (int i = 1; i <sa.length-8; i++) {
            if(sa[i].getIcon().toString().equals(sa[i-1].getIcon().toString()))
                sa[i].setIcon(new ImageIcon(getRandomImage()));
            if(sa[i].getIcon().toString().equals(sa[i+1].getIcon().toString()))
                sa[i].setIcon(new ImageIcon(getRandomImage()));
            if(sa[i].getIcon().toString().equals(sa[i+8].getIcon().toString()))
                sa[i].setIcon(new ImageIcon(getRandomImage()));
        }
        }
        for (int i = 57; i < 62; i++) {
            if(sa[i]==sa[i-1]||sa[i]==sa[i+1]){
                sa[i].setIcon(new ImageIcon(getRandomImage()));
            }
        }

        return sa;
    }
    private static String getRandomImage(){
        int index = (int) Math.floor(Math.random() * 9);
        //System.out.println("index: " + index);
        switch (index) {
            case 1:
                return "./img/b1.png";
            case 2:
                return "./img/Piedra.png";
            case 3:
                return "./img/Metal.png";
            case 4:
                return "./img/Agua.png";
            case 5:
                return "./img/BolaDePapel.png";
            case 6:
                return "./img/bola.png";
            case 7: 
                return "./img/bola2.png";
            case 8:
                return "./img/bola3.png";
            default:
                return "./img/Madera.png";
        }
    }
    
}
