/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package game;

import javax.swing.ImageIcon;

/**
 *
 * @author natal
 */
public class Gravity {
    Reproductor reproductor=new Reproductor();    
    private Square[] sa = new Square[64];
    public Gravity(Square[] sa) {
        this.sa=sa;
    }

    public Gravity() {
    }

    public Square[] getSa() {
        return sa;
    }
    

    public static Square[] RunGravity(Square[] sa, int level) throws Exception{
        //System.out.println("GRAVEDAD");
        Gravity g=new Gravity(sa);
        g.ApplyGravity();
        g.fill(level);
        return g.getSa();
    }
    
    public void ApplyGravity(){
        for (int i = 63; i > 7; i--) {
            if (sa[i].getIcon() == null) {
                if (sa[i - 8].getIcon() != null) {
                    sa[i].setIcon(sa[(i - 8)].getIcon());
                    sa[(i-8)].setIcon(null);
                }
            }

        }
        //fill();
    }

    public void fill(int level) throws Exception {
        reproductor.AbrirFichero("src/Archivos/disco.wav");
        for (int i = 0; i < 8; i++) {
            if(sa[i].getIcon()==null){
                reproductor.Play();
                sa[i].setIcon(new ImageIcon(getRandomImage(level)));
            }
                
        }
    }
    public static boolean Checkfill(Square [] sa) {
        for (int i = 0; i < 8; i++) {
            if(sa[i].getIcon()==null)
                return true;
        }
        return false;
    }
    private static String getRandomImage(int level){
        int index = (int) Math.floor(Math.random() * 9);
        //System.out.print("index: " + index);
        switch (index) {
            case 1:
                return "./img/b1.png";
            case 2:
                return "./img/Piedra.png";
            case 3:
                return "./img/Metal.png";
            case 4:
                return "./img/Agua.png";
            case 5:
                return "./img/BolaDePapel.png";
            case 6:
                return "./img/bola.png";
            case 7: 
                return "./img/bola2.png";
            case 8:
                return "./img/bola3.png";
            default:
                return "./img/Madera.png";
      
        }
    }

    
    
}
