package game;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Diego Clavel
 */
public class Archivo {

    ArrayList<Score> lista = new ArrayList();
    String[] vec, vs;
    int[] pos, punpun;
    final int n = 6;
    String fuente;

    public Archivo(ArrayList<Score> lista) {
        /*this.vec = new String[5];
        this.vs = new String[5];
        this.pos = new int[5];
        this.punpun = new int[5];
        for (int i = 0; i < n; i++) {
            lista.add(new Score("jugador" + (i + 1), (int) (Math.random() * (2000 - 1000 - 1) + 1)));
        }*/
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public Archivo() {
        this.vec = new String[n];
        this.vs = new String[n];
        this.pos = new int[n];
        this.punpun = new int[n];
    }
    
    public void AperturaTexto(){
        File fichero = new File(fuente);
        Scanner s = null;
        try {
            s = new Scanner(fichero);
            while (s.hasNextLine()) {//para colocar cada linea en un vector
                for (int i = 0; i < vec.length; i++) {
                    String linea = s.nextLine();
                    vec[i] = linea;
                }
            }
        } catch (Exception ex) {
            //System.out.println(ex.getMessage());
        } finally {
            try {//para cerrar el archivo
                if (s != null) {
                    s.close();
                }
            } catch (Exception ex2) {
                //System.out.println(ex2.getMessage());
            }
        }
    }
    
    public void GuardarTexto(String[] vs, int[] punpun){
       FileWriter fichero = null;
       PrintWriter pw = null;
        try {
            fichero = new FileWriter(fuente);
            pw = new PrintWriter(fichero);
            for (int i = 0; i < 5; i++) {
                pw.println((i+1) + " " + vs[i] + " " + punpun[i]);
            }
        } catch (IOException ex) {
            Logger.getLogger(Archivo.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if (fichero != null) {
                try {
                    fichero.close();
                } catch (IOException ex) {
                    Logger.getLogger(Archivo.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
       
    }
    
    public void NuevoJugador(Score l1) {
        AperturaTexto();
        lista.clear();
        for (int i = 0; i < 5; i++) {
            String aux[] = vec[i].split(" ");
            pos[i] = Integer.parseInt(aux[0]);
            vs[i] = aux[1];
            punpun[i] = Integer.parseInt(aux[2]);
            lista.add(new Score(vs[i], punpun[i]));
        }
        vs[5] = l1.getNom();//agregamos el jugador al vector
        punpun[5] = l1.getScore();
        for (int i = 0; i < vec.length; i++) {
            System.out.println(vs[i] + punpun[i]);
        }
        Burbuja(vs, punpun);
        GuardarTexto(vs, punpun);
    }
    
    public void ScoreFinalJugadorNuevo(int score){
        
    }

    public void creaTexto() throws IOException {
        FileWriter fichero = null;
        fichero = new FileWriter(fuente);
        fichero.close();
    }

    public void muestra(ArrayList<Score> l1) {

    }
    

    public String [] mostrarTexto() {
        AperturaTexto();
        for (int i = 0; i < 5; i++) {
            String aux[] = vec[i].split(" ");
            pos[i] = Integer.parseInt(aux[0]);
            vs[i] = aux[1];
            punpun[i] = Integer.parseInt(aux[2]);
            lista.add(new Score(vs[i], punpun[i]));
        }
        String aux[] = new String [n];
        for (int i = 0; i < 5; i++) {
            aux[i] = ((i+1) + " " + vs[i] + " " + punpun[i]); 
        }
        return aux;
    }

    

    public static void Burbuja(String[] vs, int[] punpun) {//completo
        int aux;
        String aux2;
        for (int i = 0; i < punpun.length - 1; i++) {
            for (int j = 0; j < punpun.length - i - 1; j++) {
                if (punpun[j + 1] > punpun[j]) {
                    aux = punpun[j + 1];
                    aux2 = vs[j + 1];
                    punpun[j + 1] = punpun[j];
                    vs[j + 1] = vs[j];
                    punpun[j] = aux;
                    vs[j] = aux2;
                }
            }
        }
        System.out.println("");
        for (int i = 0; i < punpun.length; i++) {
            System.out.println( vs[i] + punpun[i]);
        }
    }

    public static void main(String[] args) throws IOException {
        Archivo ar = new Archivo();
        ar.NuevoJugador(new Score("nata",5000));
    }
}
