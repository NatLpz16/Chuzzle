package game2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 *

/**
 * @author tmx
 */
public class MatrizPosicion {
    
    private final int Px,Py;

    public MatrizPosicion(int Px, int Py) {
        this.Px = Px;
        this.Py = Py;
    }
    
    public int getPx() {
        return Px;
    }

    public int getPy() {
        return Py;
    }

}

