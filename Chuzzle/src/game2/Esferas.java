package game2;


public class Esferas {
     int x;
    public String GeraEsfera(){
        switch (Juego.lvl) {
            case 5:
                 x = (int) (Math.random() * (11 - 1 + 1) + 1);
                break;
            case 4:
                 x = (int) (Math.random() * (10 - 1 + 1) + 1);
                break;
            case 3:
                 x = (int) (Math.random() * (9 - 1 + 1) + 1);
                break;
            case 2:
                 x = (int) (Math.random() * (8 - 1 + 1) + 1);
                break;
            case 1:
                 x = (int) (Math.random() * (7 - 1 + 1) + 1);
                break;
                
        }
        switch (x) {
            case 1:
                return "src/Imagenes/Bolita-Energia-Rosa.gif";
            case 2:
                return "src/Imagenes/Piedra.png";
            case 3:
                return "src/Imagenes/EsferaAzul.gif";
            case 4:
                return "src/Imagenes/Madera.png";
            case 5:
                return "src/Imagenes/EsferaNiebla.gif";
            case 6:
                return "src/Imagenes/Lava.png";
            case 7:
                 return "src/Imagenes/MultiColor.gif";
            case 8:
                 return "src/Imagenes/EsferaRasta.gif";
            case 9:
                 return "src/Imagenes/Lluvia.gif";
            case 10:
                 return "src/Imagenes/Esfera Fluyendo.gif";
            default:
                return "src/Imagenes/Metal.png";
        }
    }
}
