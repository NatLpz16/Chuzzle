package game2;


import javax.swing.JLabel;

public class MatrizInicial {

    JLabel mat[][] = new JLabel[8][9];
    int matriz[][] = new int[8][9];

    public MatrizInicial() {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 9; j++) {
                matriz[i][j] = (int) (Math.random() * (6 - 1 + 1) + 1);
            }
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 9; j++) {
                System.out.print("[" + matriz[i][j] + "] ");
            }
            System.out.println();
        }
        System.out.println("");
        System.out.println("");
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 9; j++) {
                if (j < 7 && j > 1) {
                    if (i > 1 && i < 6) {
                        while (matriz[i][j] == matriz[i][j + 2] || matriz[i][j] == matriz[i][j - 2] || matriz[i][j] == matriz[i + 2][j] || matriz[i][j] == matriz[i - 2][j]) {
                            matriz[i][j] = (int) (Math.random() * (6 - 1 + 1) + 1);
                        }
                        if (matriz[i][j] == matriz[i + 1][j] && matriz[i][j] == matriz[i - 1][j]) {
                            matriz[i][j] = (int) (Math.random() * (6 - 1 + 1) + 1);
                        }
                        if (matriz[i][j] == matriz[i][j + 1] && matriz[i][j] == matriz[i][j - 1]) {
                            matriz[i][j] = (int) (Math.random() * (6 - 1 + 1) + 1);
                        }
                    }

                }

            }
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 9; j++) {
                System.out.print("[" + matriz[i][j] + "] ");
            }
            System.out.println();
        }
    }

    public JLabel[][] CrearMatrizInicial() {

        return mat;
    }
}
