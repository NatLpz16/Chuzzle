package game2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author natal
 */
public class Gravity {

    JLabel mat[][] = new JLabel[8][9];
    MatrizPosicion mat2[][] = new MatrizPosicion[8][9];

    public Gravity(JLabel mat[][], MatrizPosicion mat2[][]) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 9; j++) {
                this.mat[i][j] = mat[i][j];
            }
        }
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 9; j++) {
                this.mat2[i][j] = mat2[i][j];
            }
        }

    }

    public JLabel[][] getMat() {
        return mat;
    }

    public static JLabel[][] RunGravity(JLabel matr[][], MatrizPosicion matr2[][]) {
        Gravity g = new Gravity(matr, matr2);
        g.ApplyGravity();
        g.fill();
        return g.mat;
    }

    public void ApplyGravity() {
        int x = 1;
        for (int i = 7; i > -1; i--) {
            for (int j = 8; j > -1; j--) {
                for (int k = 0; k < 8; k++) {
                    for (int l = 0; l < 9; l++) {
                        if (mat2[i][j].getPx() == mat[k][l].getX() && mat2[i][j].getPy() == mat[k][l].getY()) {
                            if (mat[k][l].getIcon() == null) {
                                for (int m = 0; m < 8; m++) {
                                    for (int n = 0; n < 9; n++) {
                                        if (mat2[i][j].getPx() == mat[m][n].getX() && mat2[i][j].getPy() - 81 == mat[m][n].getY()) {
                                            if (mat[m][n].getIcon() != null) {
                                                mat[k][l].setIcon(mat[m][n].getIcon());
                                                mat[m][n].setIcon(null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public void fill() {
        
        for (int j = 0; j < 9; j++) {
            
            for (int k = 0; k < 8; k++) {
                
                for (int l = 0; l < 9; l++) {
                    
                    if (mat2[0][j].getPx() == mat[k][l].getX() && mat2[0][j].getPy() == mat[k][l].getY()) {
                        
                        if (mat[k][l].getIcon() == null) {
                            mat[k][l].setIcon(new ImageIcon(new Esferas().GeraEsfera()));
                        }
                    }
                }
            }
        }

    }
    public static boolean Checkfill(JLabel[][] mat) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 9; j++) {
                if (mat[i][j].getIcon() == null) {
                    return false;
                }
            }
        }
        return true;
    }

}
