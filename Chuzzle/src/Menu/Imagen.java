/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author natal
 */
public class Imagen extends javax.swing.JPanel{
    public Imagen(){
        this.setSize(1300,800);
    }
    public void paint(Graphics grafico){
        Dimension height = getSize();
        ImageIcon img = new ImageIcon(getClass().getResource("src/Imagenes/f.jpg"));
        grafico.drawImage(img.getImage(), 0,0,height.width,height.height,null);
        setOpaque(false);
        super.paintComponent(grafico);
    }
}
